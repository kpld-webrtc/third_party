// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef THIRD_PARTY_BLINK_RENDERER_BINDINGS_CORE_V8_TO_V8_TRAITS_H_
#define THIRD_PARTY_BLINK_RENDERER_BINDINGS_CORE_V8_TO_V8_TRAITS_H_

#include "third_party/blink/renderer/bindings/core/v8/idl_dictionary_base.h"
#include "third_party/blink/renderer/platform/bindings/callback_function_base.h"
#include "third_party/blink/renderer/platform/bindings/callback_interface_base.h"
#include "third_party/blink/renderer/platform/bindings/dictionary_base.h"
#include "third_party/blink/renderer/platform/bindings/dom_data_store.h"
#include "third_party/blink/renderer/platform/bindings/enumeration_base.h"
#include "third_party/blink/renderer/platform/bindings/script_wrappable.h"
#include "third_party/blink/renderer/platform/bindings/v8_binding.h"
#include "v8/include/v8.h"

namespace blink {

// ToV8Traits provides C++ -> V8 conversion.
// Currently, you can use ToV8() which is defined in to_v8.h for this
// conversion, but it cannot throw an exception when an error occurs.
// We will solve this problem and replace ToV8() in to_v8.h with
// ToV8Traits::ToV8().
// TODO(canonmukai): Replace existing ToV8() with ToV8Traits<>.

// Primary template for ToV8Traits.
template <typename T, typename SFINAEHelper = void>
struct ToV8Traits;

// ScriptWrappable
template <typename T>
struct ToV8Traits<
    T,
    typename std::enable_if_t<std::is_base_of<ScriptWrappable, T>::value>> {
  static v8::MaybeLocal<v8::Value> ToV8(ScriptState* script_state,
                                        ScriptWrappable* impl)
      WARN_UNUSED_RESULT {
    return ToV8(script_state->GetIsolate(), impl,
                script_state->GetContext()->Global());
  }

  // This overload is used for the case when the ToV8() caller already has
  // a receiver object (a creation context object) which is needed to create
  // a wrapper.
  static v8::MaybeLocal<v8::Value> ToV8(v8::Isolate* isolate,
                                        ScriptWrappable* impl,
                                        v8::Local<v8::Object> creation_context)
      WARN_UNUSED_RESULT {
    if (UNLIKELY(!impl)) {
      return v8::Null(isolate);
    }
    v8::Local<v8::Value> wrapper = DOMDataStore::GetWrapper(impl, isolate);
    if (!wrapper.IsEmpty()) {
      return wrapper;
    }

    if (!impl->WrapV2(isolate, creation_context).ToLocal(&wrapper)) {
      return v8::MaybeLocal<v8::Value>();
    }
    return wrapper;
  }
};

// Dictionary
template <typename T>
struct ToV8Traits<T,
                  typename std::enable_if_t<
                      std::is_base_of<bindings::DictionaryBase, T>::value>> {
  static v8::MaybeLocal<v8::Value> ToV8(
      ScriptState* script_state,
      const bindings::DictionaryBase* dictionary) WARN_UNUSED_RESULT {
    DCHECK(dictionary);
    v8::Local<v8::Value> v8_value = dictionary->CreateV8Object(
        script_state->GetIsolate(), script_state->GetContext()->Global());
    DCHECK(!v8_value.IsEmpty());
    return v8_value;
  }
};

// Old implementation of Dictionary
template <typename T>
struct ToV8Traits<
    T,
    typename std::enable_if_t<std::is_base_of<IDLDictionaryBase, T>::value>> {
  static v8::MaybeLocal<v8::Value> ToV8(ScriptState* script_state,
                                        const IDLDictionaryBase* dictionary)
      WARN_UNUSED_RESULT {
    DCHECK(dictionary);
    return dictionary->ToV8Impl(script_state->GetContext(),
                                script_state->GetIsolate());
  }
};

// Callback function
template <typename T>
struct ToV8Traits<T,
                  typename std::enable_if_t<
                      std::is_base_of<CallbackFunctionBase, T>::value>> {
  static v8::MaybeLocal<v8::Value> ToV8(ScriptState* script_state,
                                        CallbackFunctionBase* callback)
      WARN_UNUSED_RESULT {
    // creation_context (|script_state->GetContext()|) is intentionally ignored.
    // Callback functions are not wrappers nor clonable. ToV8 on a callback
    // function must be used only when it's in the same world.
    DCHECK(callback);
    DCHECK(&callback->GetWorld() ==
           &ScriptState::From(script_state->GetContext())->World());
    return callback->CallbackObject().As<v8::Value>();
  }
};

// Callback interface
template <typename T>
struct ToV8Traits<T,
                  typename std::enable_if_t<
                      std::is_base_of<CallbackInterfaceBase, T>::value>> {
  static v8::MaybeLocal<v8::Value> ToV8(ScriptState* script_state,
                                        CallbackInterfaceBase* callback)
      WARN_UNUSED_RESULT {
    // creation_context (|script_state->GetContext()|) is intentionally ignored.
    // Callback Interfaces are not wrappers nor clonable. ToV8 on a callback
    // interface must be used only when it's in the same world.
    DCHECK(callback);
    DCHECK(&callback->GetWorld() ==
           &ScriptState::From(script_state->GetContext())->World());
    return callback->CallbackObject().As<v8::Value>();
  }
};

// Enumeration
template <typename T>
struct ToV8Traits<T,
                  typename std::enable_if_t<
                      std::is_base_of<bindings::EnumerationBase, T>::value>> {
  static v8::MaybeLocal<v8::Value> ToV8(
      ScriptState* script_state,
      const bindings::EnumerationBase& enumeration) WARN_UNUSED_RESULT {
    return V8String(script_state->GetIsolate(), enumeration.AsCStr());
  }
};

}  // namespace blink

#endif  // THIRD_PARTY_BLINK_RENDERER_BINDINGS_CORE_V8_TO_V8_TRAITS_H_
