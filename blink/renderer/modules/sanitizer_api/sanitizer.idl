// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// https://github.com/WICG/sanitizer-api

typedef (DOMString or DocumentFragment or Document) SanitizerInput;
typedef (DOMString or TrustedHTML or DocumentFragment or Document) SanitizerInputWithTrustedHTML;

[
  Exposed=Window,
  RuntimeEnabled=SanitizerAPI
] interface Sanitizer {
  [RaisesException] constructor(optional SanitizerConfig config = {});
  [CallWith=ScriptState, RaisesException] DOMString sanitizeToString(SanitizerInput input);
  [CallWith=ScriptState, RaisesException] DocumentFragment sanitize(SanitizerInputWithTrustedHTML input);
};
