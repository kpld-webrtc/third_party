{
  "layers": [
    {
      "name": "Scrolling background of LayoutView #document",
      "bounds": [800, 600],
      "contentsOpaque": true,
      "backgroundColor": "#FFFFFF"
    },
    {
      "name": "LayoutNGTableRow TR",
      "bounds": [73, 23],
      "backgroundColor": "#008000",
      "invalidations": [
        [0, 0, 37, 23]
      ],
      "transform": 1
    }
  ],
  "transforms": [
    {
      "id": 1,
      "transform": [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [9, 9, 0, 1]
      ]
    }
  ]
}

