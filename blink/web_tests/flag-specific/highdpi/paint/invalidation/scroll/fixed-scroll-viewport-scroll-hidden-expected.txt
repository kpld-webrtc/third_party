{
  "layers": [
    {
      "name": "Scrolling Contents Layer",
      "bounds": [1200, 3024],
      "contentsOpaque": true,
      "backgroundColor": "#FFFFFF",
      "invalidations": [
        [12, 300, 150, 150],
        [12, 150, 150, 150]
      ],
      "transform": 1
    }
  ],
  "transforms": [
    {
      "id": 1,
      "transform": [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, -150, 0, 1]
      ]
    }
  ]
}

