{
  "layers": [
    {
      "name": "Scrolling Contents Layer",
      "bounds": [1200, 900],
      "contentsOpaque": true,
      "backgroundColor": "#FFFFFF",
      "invalidations": [
        [677, 241, 210, 209],
        [268, 241, 210, 209],
        [64, 241, 209, 209],
        [477, 245, 205, 205]
      ]
    }
  ]
}

