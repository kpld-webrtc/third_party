{
  "layers": [
    {
      "name": "Scrolling Contents Layer",
      "bounds": [1200, 900],
      "contentsOpaque": true,
      "backgroundColor": "#FFFFFF",
      "invalidations": [
        [282, 16, 78, 23],
        [18, 16, 78, 23],
        [270, 13, 6, 27]
      ]
    }
  ]
}

