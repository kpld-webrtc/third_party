{
  "layers": [
    {
      "name": "Scrolling Contents Layer",
      "bounds": [1200, 900],
      "contentsOpaque": true,
      "backgroundColor": "#FFFFFF"
    },
    {
      "name": "LayoutTableRow TR id='target'",
      "position": [12, 141],
      "bounds": [277, 96],
      "backgroundColor": "#ADD8E6",
      "invalidations": [
        [0, 0, 277, 96]
      ]
    },
    {
      "name": "LayoutNGTableCell TD",
      "bounds": [88, 96],
      "transform": 1
    }
  ],
  "transforms": [
    {
      "id": 1,
      "transform": [
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [107, 141, 0, 1]
      ]
    }
  ]
}

